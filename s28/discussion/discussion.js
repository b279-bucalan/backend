// CRUD Operations 

/*
    - CRUD operations are the heart of any backend application.
    - Mastering the CRUD operations is essential for any developer.
    - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
    - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
*/

// [SECTION] Inserting documents (Create)

// Intsert One Document

/*
    - Since mongoDB deals with objects as it's structure for documents, we can easily create them by providing objects into our methods.
    - The mongo shell also uses JavaScript for it's syntax which makes it convenient for us to understand it's code
    - Creating MongoDB syntax in a text editor makes it easy for us to modify and create our code as opposed to typing it directly in the terminal where the whole code is only visible in one line.

    - By using a text editor it allows us to type the syntax using multiple lines and simply copying and pasting the code in terminal will make it work.
    - Syntax
        - db.collectionName.insertOne({object});
    - JavaScript syntax comparison
        - object.object.method({object});
*/

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "09123456789",
		email: "janedoe@mail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	depatment: "none"
});

// Insert Many
/*
Syntax
- db.collectionName.insertMany([{object A}, {objectB}]);

*/

db.users.intertMany([
	{
	firstName: "Stephen",
	lastName: "Hawking",
	age: 76,
	contact: {
		phone: "09123456789",
		email: "stephenhawking@mail.com"
	},
	courses: ["Python", "React", "PHP"],
	depatment: "none"
	},
	{
	firstName: "Neil",
	lastName: "Armstrong",
	age: 82,
	contact: {
		phone: "09123456789",
		email: "neilarmstrong@mail.com"
	},
	courses: ["React", "Laravel", "Sass"],
	depatment: "none"
	}


	]);

// Finding a single document
// Leaving the search criteria emply will retrieve all documents

// SELECT * FROM users;

db.users.find();

db.users.find(
	{firstName: "Stephen"}
	);

// The "pretty" method allows us to be able to view documents returned by our terminal in a better format.

db.users.find({firstName: "Stephen"}).pretty();

db.users.find(

	{lastName: "Armstrong"}, {age: 82}
	
	);

// [SECTION] UPdating documents (Update);

// Updating singe document
// Creating dummy user to update
db.users.insertOne({
	firstName: "test",
	lastName: "test",
	age: 0,
	contact: {
		phone: "00000000000",
		email: "test@mail.com"
	},
	courses: [],
	department: "none"
});

/*
Syntax

	db.collectionName.updateOne({criteria}, {$set: {field: value}});


*/

db.users.updateOne(
	{ firstName: "Test"}, 
	{$set: {
		firstName: "Bill",
		lastName: "Gates",
		age: 62,
		contact:{
			phone: "09123456789",
			email: "bill@mail.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations",
		status: "active"
	}}
);

db.users.find(
	{firstName: "Bill"}
	);

// Updating many documents

db.users.updateMany(
	{ depatment: "none"},
	{$set : {
		depatment: "HR"
	}
}
);

db.users.updateMany(
	{ department: "none"},
	{$set : {
		department: "HR"
	}
}
);

// Replace One
/*
Syntax
	db.collectionName.replaceOne({criteria}, {field: value});

*/

db.users.replaceOne(
	{ firstName: "Bill" },
	{
	firstName: "Bill",
	lastName: "Gates",
	age: 62,
	contact:{
		phone: "09123456789",
		email: "bill@mail.com"
	},
	courses: ["PHP", "Laravel", "HTML"],
	department: "Operations",
	}
	
);

// [SECTION] Deleting Documents (Delete)

db.users.insert({
	firstName: "test"
});

// Delete Many
db.users.deleteMany({
	firstName: "test"
});

// Delete One
db.users.deleteOne({
	firstName: "test"
});

// [SECTION] Advanced Queries

db.users.find({
	contact: {
		phone: "09123456789",
		email: "stephenhawking@mail.com"
	}
});

// Query on nested field /dot notation

db.users.find({
	"contact.email": "stephenhawking@mail.com"

});

// Querying an Array without regard to order
db.users.find({courses: {$all: ["React","Python"]}});
db.users.find({courses: {$all: ["React"]}});

// Querying and embeded Array

db.users.insertOne({
	nameArray: [{nameA: "Juan"}, {nameB: "Tamad"}]
});