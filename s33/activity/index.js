fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => res.json())
.then(json => console.log(json));


fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => res.json())
.then(title => {
	const titleMap = title.map(dataToMap => dataToMap.title);
	console.log(titleMap);
});


fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(res => res.json())
.then(result => console.log(result));




// #7
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		completed: false,
		id: 201,
		title: "Created To Do List Item",
		userId: 1
	})
})
.then(res => res.json())
.then(json => console.log(json));


// #8
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		id: 1,
		status: "Pending",
		title: "Updated To Do List Item",
		userId: 1
	})
})
.then(res => res.json())
.then(json => console.log(json));


// #9
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		completed: false,
		dateCompleted: "07/09/21",
		id: 1,
		status: "Complete",
		title: "delectus aut autem",
		userId: 1
	})
})
.then(res => res.json())
.then(json => console.log(json));



fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
});