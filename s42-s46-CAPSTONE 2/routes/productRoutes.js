const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers.js");
const auth = require("../auth.js");


module.exports = router;

// Route for creating a course
router.post("/", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});
// route for all products
router.get("/allProducts",(req,res)=> {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
})
// route for getting all active products
router.get('/available', (req,res)=>{
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
})

// Retrieve specific product
router.get('/:productId',(req, res)=>{
	console.log(req.params.productId);
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})
// Update specific product
router.put('/:productId', auth.verify, (req, res)=>{
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.updateProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
})

// archive a product
router.put('/:productId/archive', auth.verify, (req, res)=>{
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.archiveProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
})
// activated a product
router.put('/:productId/activate', auth.verify, (req, res)=>{
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.activateProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
})