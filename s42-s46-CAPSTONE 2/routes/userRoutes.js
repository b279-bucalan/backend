const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers.js");
const auth = require("../auth.js");

// route for checking if email already exist.
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// route for registering
router.post("/register", (req, res) =>{ 
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));

})

// route for user login
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Order of user to a product
router.post("/checkout", (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		products : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	userController.checkout(data).then(resultFromController => res.send(resultFromController));
})


// Retrieve all user details
router.get('/allUsers', auth.verify, (req, res)=>{
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.allUsers(isAdmin).then(resultFromController => res.send(resultFromController));
})

// STRETCH GOAL #1

// Set user as admin (admin only)
router.put('/:userId/userAdmin', auth.verify, (req, res)=>{
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.userAdmin(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
})


module.exports = router;