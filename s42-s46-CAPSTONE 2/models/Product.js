const mongoose = require("mongoose");
const productSchema = new mongoose.Schema({
	name : {
		type: String,
		required : [true, "Product Name is required!"]
	},
	description: {
		type : String,
		required : [true, "Product description is required!"]
	},
	price: {
		type : Number,
		required : [true, "Product price is required!"]
	},
	isActive: {
		type : Boolean,
		default : true
	},
	createdOn: {
		type : Date,
		// getting the exact date
		default : new Date()
	},
	userOrders: [
		{
			userId : {
				type : String,
				required : [true, "USERID is required!"]
			},
			orderId : {
				type : String,
				required : [false, "OrderId is required"]
			}
		}
	]

})

module.exports = mongoose.model("Product", productSchema);