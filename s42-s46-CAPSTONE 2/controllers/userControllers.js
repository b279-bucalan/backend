const User = require("../models/User");
const bcrypt = require("bcrypt");
const Product = require("../models/Product");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result=> {
		if(result.length > 0) {
			return "email exist"
		}else {
			return false
		}
	})
}

// register a user
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		// 10 is the value provided as the number of salt rounds.
		password: bcrypt.hashSync(reqBody.password, 10)
	})



	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		}else {
			return "Thank you for creating an account";
		}
	})
};

// function to login a user
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false
		}else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			}else {
				return false
			}
		}
	})
}

module.exports.checkout = async (data) => {
  if (data.isAdmin) {
    return "ADMIN cannot do this action.";
  } else {
    const user = await User.findById(data.userId);
    const newOrderedProduct = {
      products: [],
      totalAmount: 0,
    };
    for (const productData of data.products) {
      const product = await Product.findById(productData.productId);
      if (!product) {
        return `Product with ID ${productData.productId} not found`;
      }
      newOrderedProduct.products.push({
        productId: productData.productId,
        quantity: productData.quantity,
        productName: product.name,
      });
      console.log(product.name)
      newOrderedProduct.totalAmount += product.price * productData.quantity;
      product.userOrders.push({ userId: data.userId });
      await product.save();
    }
    user.orderedProduct.push(newOrderedProduct);
    await user.save();
    return "Successfully ordered!";
  }
};

// Retrieve all user details
module.exports.allUsers = (isAdmin) => {
	console.log(isAdmin);


	if(isAdmin){
	return User.find({}).then((users, error) => {
		if(error){
			return false;
		}else{
			return users;
		}
	})

	}let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})

}
// STRETCH GOAL #1

// SET USER AS ADMIN (ADMIN ONLY)
module.exports.userAdmin = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);


	if(isAdmin){
		let userAsAdmin = {
		isAdmin : reqBody.isAdmin
	}
	return User.findByIdAndUpdate(reqParams.userId, userAsAdmin).then((user, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})

}
