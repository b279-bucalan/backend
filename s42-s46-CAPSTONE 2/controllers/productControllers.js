const Product = require("../models/Product")

module.exports.addProduct = (data) => {
	console.log(data.isAdmin);
	if(data.isAdmin) {
		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		})

		return newProduct.save().then((product, error) => {
			if(error){
				return error;
			}
			return product;
		})
	} 

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
}

// retrieve all products function
module.exports.getAllProducts = () => {
	return Product.find({}).then(result =>{
		return result;
	})
}

// retrieve all active courses function
module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then(result =>{
		return result;
	})
}

// retrieving specific product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result =>{
		return result;
	})
}

// Update a specific product
module.exports.updateProduct = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);


	if(isAdmin){
		let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	}
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if(error){
			return false;
		}else{
			return `Product updated!`;
		}
	})
}let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})

}

// archive a specific product
module.exports.archiveProduct = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);


	if(isAdmin){
		let archiveProduct = {
		isActive : reqBody.isActive
	}
	return Product.findByIdAndUpdate(reqParams.courseId, archiveProduct).then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})

}

// activate a specific product
module.exports.activateProduct = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);


	if(isAdmin){
		let activatedProduct = {
		isActive : reqBody.isActive
	}
	return Product.findByIdAndUpdate(reqParams.productId, activatedProduct).then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})

}
